#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/prctl.h>

// http://www.alexonlinux.com/signal-handling-in-linux
void sig_handler(int signum) {
  printf("%d received signal %d\n", getpid(), signum);
  //wait();
  //pause();
}

void sig_term(int signum) {
  // singal must be handled and exit() call to exit parent and let childern run
  exit(0);
}

main (int argc, char* argv[]) {
 signal(SIGCHLD, sig_handler);
 signal(SIGHUP, sig_handler);
 signal(SIGTERM, sig_term);

 // Change process name shown by ps
 prctl(PR_SET_NAME, "A");
 argv[0] = "A";

 if (fork() == 0) {
  prctl(PR_SET_NAME, "B");
  argv[0] = "B";
  pause(); // B
  //exit(0);
 }

 if (fork() == 0) {
  if (fork() == 0) {
   prctl(PR_SET_NAME, "D");
   argv[0] = "D";
   pause(); // D
   //exit(0);
  }

  prctl(PR_SET_NAME, "C");
  argv[0] = "C";
  pause(); // C
  //exit(0);
 }

 pause(); // A
 exit(0);
}

# Must be run under root (sudo)
if [ `whoami` != "root" ]; then 
  echo "Must be run under root"
  exit 1
fi

groupadd student
useradd -G student novy
passwd -l novy
useradd -G student novak
passwd -l novak

groupadd pedagog
useradd -G pedagog dvorak
passwd -l dvorak
useradd -G pedagog smetana
passwd -l smetana

chown novy:student prog1
chown dvorak:pedagog prog2
chown smetana:pedagog prog3
chown novak:student prog4

chmod 1765 prog1
chmod 5764 prog2
chmod 4765 prog3
chmod 4665 prog4

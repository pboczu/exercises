#include <stdio.h>
#include <fcntl.h>

void  print_eff_credentials(char*);

int main(int argc, char *argv[]) {
  print_eff_credentials("Prog4");
  // printf("%d", argc);
  if (argc == 2) {
    int fd = open(argv[1], O_WRONLY | O_APPEND | O_CREAT, 0664);
    //printf("%s\n", argv[1]);
    //printf("%d\n", fd);    
  if (fd != -1) { 
      char *buf = "appended from prog4\n";
      write(fd, buf, sizeof(buf));
      close(fd);
    } else {
      printf("Error opening file.");
      fflush(stdout);
      return 1;
    }
  }

  fflush(stdout);
  return 0;
}

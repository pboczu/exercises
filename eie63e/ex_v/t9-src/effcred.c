#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include<stdio.h>

char* resUsrName(uid_t uid)
{
  struct passwd *pws;
  pws = getpwuid(uid);
  return pws->pw_name;
}

char* resGrpName(gid_t gid)
{
  struct group *grp;
  grp = getgrgid(gid);
  return grp->gr_name;
}

void print_eff_credentials(char* prgname) { 
  printf(
   "%s lanched as %s/%s\n", 
   prgname,
   resUsrName(geteuid()),
   resGrpName(getegid()));
}

/*main() {
  //printf("real=%s/%s\n", resUsrName(getuid()),resGrpName(getgid()));
  //printf("effctive=%s/%s\n", resUsrName(geteuid()),resGrpName(getegid()));
  printf("%s/%s\n", resUsrName(geteuid()),resGrpName(getegid()));
}*/


#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

void sig_handler(int signum) {
  printf("In hanlder");
  signal(SIGTERM, SIG_DFL); 
}

main () {
 
 if (fork() == 0) {
  //printf ("n/a");
  exit(0);
 }

 signal(SIGCONT, sig_handler);
 signal(SIGTERM, sig_handler);
 pause();
 printf("After pause 1");
 pause();
 printf("After pause 2");
}

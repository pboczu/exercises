# 1
echo -n = no new line  
Last echo to issue new line, otherwise output is stick to prompt.  
S # [1-print.a.sh](1-print.a.sh)  
S * [1-print.b.sh](1-print.b.sh)

# 2
Netestovat, že je to číslo, příliš složité.  
[2-pocitejdo.sh](2-pocitejdo.sh)

# 3
Pro skryté buď použít expanzní výraz, nebo find, ls neumí vracet absolutní cesty.  
Řešení přes expanzní výrazy nefunguje, musejí na to být dva listy a ty spojit není snadné.  
zadání [3-count.a.sh](3-count.a.sh)  
skryté [3-count.b.sh](3-count.b.sh)  
skryté přes expanzní [3-count.c.sh](3-count.c.sh)  
symlinky [3-count.d.sh](3-count.d.sh)  
while -> for [3-count.e.sh](3-count.e.sh)  

# 4
chmod 7654 kopie  
chmod 5757 vypis  
Lze spustit vypis

# 5
Aby jiný viděl do adersáře, musím nejprve dát práva na home, e.g.  ~ chmod 755 . (ta tečka je důležitá, je to aktuální adresář = můj domovský)  
chmod 600 data  
chmod 4755 vypis

# 6
Konec řádku = LF = 0a.  
Konec souboru žádný vidět není.

# 7
strings -n 8  /bin/cp | grep -i copyright

#!/bin/bash
#nums : script vypise pocet souboru a adresaru
if [ $# -eq 0 ]
 then dir="."
 else dir=$1
fi
while :
do
 if [ ! -d $dir ]
 then echo "$0 : $dir neni adresar"
 else
 echo "$dir :"
 fcount=0
 dcount=0
 lcount=0
 for file in `find $dir -maxdepth 1`
 do
 if [ -f $file ]
  then fcount=`expr $fcount + 1`
 fi
 if [ -d $file ]
 then dcount=`expr $dcount + 1`
 fi
 if [ -L $file ]
 then lcount=`expr $lcount + 1`
 fi
 done
 echo $dcount adresaru $fcount obycejnych souboru, z toho $lcount linku
 fi
 if [ $# -le 1 ]
 then break
 else
 shift
 dir=$1
 fi
done

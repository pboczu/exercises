#!/bin/bash
if [ $# -ne 1 ]; then echo "Wrong number of arguments"; exit 1; fi;
[ $1 -eq 0 ] >/dev/null 2>&1;
if [ $? -gt 1 ]; then echo "Argument is not a number"; exit 1; fi;

a=1

while [ $a -le $1 ]
do
  echo $a;
  a=`expr $a + 1`;
done;

/* cat: version 1 Print file on terminal */
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
 
  main(argc,argv)
    int argc;
    char *argv[];
    { 
      int d,count;
      char buf[1024];
        if(argc != 2) {
          printf("error: cat must have one parametr\n");
          exit(1);
        }
        d = open(argv[1],O_RDONLY);
        if(d == -1) {
          printf("cannot open file %s\n", argv[1]);
          exit(1);
        }
        while(( count = read(d,buf,sizeof(buf))) > 0) {
          write(1,buf,count);
        }
        return 0 ;
     }


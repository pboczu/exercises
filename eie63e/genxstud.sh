#!/bin/bash

# Generates another set of student accounts xstudent00..xstudent30i
if [ "$EUID" -ne 0 ]; then echo "Must be run as root"; exit 1; fi;

for i in {0..30}
 do 
  N=$(printf "%02d" $i)
  UN=xstudent$N
  sudo useradd $UN
  echo student | passwd --stdin $UN

 done;

#include <stdio.h>

#define CMASK 0377 // ensures read c is positive

int mygetchar() {
 char c;
 return (read(0, &c, 1) > 0 ? c & CMASK : EOF);
}

void myputchar(int ch)
{
  char c = ch & CMASK;
  write(1, &c, 1);
} 


main() {
 //int ch = mygetchar();
 //printf("Read %d\n", ch);
 myputchar('a');
}

#include <signal.h>
#include <stdio.h>

void handler(int sig) {
 if (sig == 2) {
  printf("signalem 2 mne nelze zrusit\n");
 }
 if (sig == 14) {
  printf("tik-tak\n");
 }

 // In past or other version of kernel, handler must be re-registered, 
 // in our version, the handler remains registered until deregistered.
 //signal(2,handler);
 //signal(14,handler);
}

main() {
 signal(2,handler);
 signal(14,handler);
 
 int i; 
 for(i=0;i<100;i++) {
  // Combination of alarm() - send signal 14, see registering hanler above
  // and pause() works same way as sleep{].
  alarm(5);
  pause();
 }
}

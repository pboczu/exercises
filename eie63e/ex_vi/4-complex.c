#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <errno.h>

void sighandler(int signum) {
  exit(0); 
}

int main (int argc, char* argv[]) {
  printf("-- in main() --\n");

 // 0 = itself, 1 = 1st arg
 if (argc != 2) {
  printf("Wrong number of args.\n");
  exit(1);
 }

 printf("-- about parse --\n");

 // https://linux.die.net/man/3/strtol
 // errno = 0; // errno cannot be used, endptr must be, see https://stackoverflow.com/questions/9445171/strtol-using-errno
 char* endptr;
 int secs = strtol(argv[1], &endptr, 10);
 
 printf("-- secs = %d, errno = %d\n", secs, errno);

 // In fact, errno is not filled and is ever 0, even if input is not a number."
 if (endptr == argv[1]) {
  printf("Arg is not a number.\n");
  exit(1);
}

 printf("-- parsed --\n");

 if (secs) { 
  // Event signal handling...
  signal(SIGALRM, sighandler);
  alarm(secs);
  pause();
  
  // ... or simple sleep
  //sleep(secs);
 }
}

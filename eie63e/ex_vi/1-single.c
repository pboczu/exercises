#include <signal.h>
#include <stdio.h>

void handler(int sig) {
 if (sig == SIGINT) {
  printf("signalem 2 mne nelze zrusit\n");
  signal(SIGINT, SIG_DFL);
 }
}

main() {
 signal(SIGINT,handler);
 
 int i; 
 for(i=0;i<100;i++) {
  sleep(5);
 
  printf("tik-tak\n");
 }
}

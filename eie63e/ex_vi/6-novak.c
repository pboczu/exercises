#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#define CMASK 0377 // ensures read c is positive

int mygetchar() {
 char c;
 return (read(0, &c, 1) > 0 ? c & CMASK : EOF);
}

void myputchar(int ch)
{
  char c = ch & CMASK;
  write(1, &c, 1);
} 


main() {
 int c,fd1;
 // origin /home/... is replaced with /var/tmp/...
 if((fd1=open("/var/tmp/novak/data",O_RDONLY))<0) {
    printf("soubor nelze otevrit\n");
    exit(1);
 }
 else {
  close(0);
  dup(fd1);
  close(fd1);
 }

 while((c=mygetchar()) != EOF) {
  myputchar(c);
 }

 return 0; 
}

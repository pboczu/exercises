#include <stdio.h>
#include <stdlib.h>

#include <unistd.h> // Must be added to make execl available

main()
{
 int pid,status;
 char *padr;
 if ((pid = fork()) == 0){
  if ((padr=getenv("ADR")) == NULL) padr=".";
  execl("/bin/ls","ls","-al",padr,NULL);
  exit(1);
 }
 
 wait(&status);
 if(status){
  printf("ls nebyl spusten nebo skoncil s chybou (%d)\n", status);
  exit(1);
 }
 else {
  printf("program ls skoncil O.K. \n");
  exit(0);
 }
} 


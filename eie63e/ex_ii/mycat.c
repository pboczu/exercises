/* cat: verse 2. Rozsireni verse 1 */
#include <fcntl.h>
#include <stdio.h>

extern int copy();

main(int argc, char* argv[])
{
	int i,d; 
	if(argc == 1) {
		copy(0);
	}
	else {
		for(i=1;i<argc;i++){
			if ((d=open(argv[i], O_RDONLY)) == -1) {
				printf("can’t open file %s\n",argv[i]);
				break;
			}
			copy(d);
			close(d);
		}
	}
}

int copy(int d)
{
	int count;
	char buf[1024];
	while(( count = read(d,buf,sizeof(buf))) > 0)
		write(1,buf,count);
} 

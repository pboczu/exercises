#!/bin/bash

if [ $# -eq 0 ]
 then 
  echo "Student number or terminal name expected."
  exit 1
fi

if [[ $1 =~ "pts" ]]
 then
  # arg is pts, use it directly
  pts=$1
 else
  # arg is student number, find pts
  ptys=$(who | grep student$1 | awk '{print $2}')
  # count words, becase terminals are in $ptys listed in single line
  nptys=$(echo $ptys | wc -w) 
  if [ $nptys -eq 0 ]
   then 
    echo Student $1 not found.
    exit 1
  elif [ $nptys -gt 1 ]; then
   echo More terminals found: $ptys
   exit 1
  else
   # just one terminal found, use it
   pts=$ptys
  fi
fi

if [ $# -gt 1 ]; then
 shift # consume student number/pts
 fds=$*
else
 fds="0 1 2"
fi

echo Spying on terminal $pts on fd $fds
# see https://superuser.com/a/781973
sudo peekfd -8cnd $(ps fat | grep "$pts *Ss" | awk '{print$1}') $fds
